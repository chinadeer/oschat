/**
 * Created by clarie on 14-7-21.
 */
(function(){
    oschat.Stat=Stat;
    var EXTEND = null;

    function Stat(){
        if(EXTEND){
            EXTEND.apply(this,arguments);
        }

        this._subHolder=$('#user .sub-holder');

        this._lis=$('#user .dropdown-menu li');

        var self=this;

        this._lis.click(function(){
            var index=self._lis.index($(this)[0]);
            self.setStatus(index);
        });
    }

    Stat.prototype={
        init:function(){

        },
        setStatus:function(index){
            switch (index){
                case 0:{
                    this._subHolder.css('background-position','1px 1px');
                    break;
                }
                case 1:{
                    this._subHolder.css('background-position','1px -143px');
                    break;
                }
                case 2:{
                    break;
                }
                case 3:{
                    this._subHolder.css('background-position','1px -116px');
                    break;
                }
            }
        }
    }
})();
